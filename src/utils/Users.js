import Config from '../config';


export default {
    getCount(){
        return fetch(Config.apiPath+'/users/count',{
            headers:{
                'Accept': 'application/json',
            }
            ,
            method:'GET',
        })
    },
    checkToken(token){
        return fetch (Config.apiPath+'/users/checktoken',{
            headers:{
                'Authorization':"Bearer "+token,
            },
            method:'GET',
        })
    },
    getAuthorizedUser(token){
        return fetch (Config.apiPath+'/users/authorized',{
            headers:{
                'Authorization':"Bearer "+token,
            },
            method:'GET',
        })
    },
    changePassword(newPassword,oldPassword,token){
        const data={
            new:newPassword,
            old:oldPassword,
        }

        return fetch(Config.apiPath+'/users/changepassword',{
            headers:{
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token,
            },
            method:'POST',
            body: JSON.stringify(data),
        });
    },
    performSingUp(userData){


        return fetch(Config.apiPath+'/users/signup',{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              method: "POST",
              body: JSON.stringify(userData)
        });
    },

    performLogin(loginData){


        return fetch(Config.apiPath+'/users/login',{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
              method: "POST",
              body: JSON.stringify(loginData)
        });
    },

    getAll(){
        
        return fetch(Config.apiPath+'/users',{
                headers: {
                'Accept': 'application/json',
              },
              method: "GET",
              
        });
    },
  
    getOne(id_or_username){
        return fetch(Config.apiPath+'/users/'+id_or_username,{
                headers: {
                'Accept': 'application/json',
              },
              method: "GET",
              
        });
    },

    updateUser(userId,userData,token){


        return fetch(Config.apiPath+'/users/'+userId,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token,
              },
              method: "PUT",
              body: JSON.stringify(userData)
        });
    },
    deleteUser(userId,token){


        return fetch(Config.apiPath+'/users/'+userId,{
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer '+token,
              },
              method: "DELETE",
             
        });
    },

}