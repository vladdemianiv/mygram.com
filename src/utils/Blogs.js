import Config from '../config';


export default {
    createBlog(blogData,token){


        return fetch(Config.apiPath+'/blogs',{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+ token,

              },
              method: "POST",
              body: JSON.stringify(blogData)
        });
    },
    getAuhtorizedUserBlogs(token){
        return fetch(Config.apiPath+'/blogs/authorized',{
            headers:{
                'Accept':'application/json',
                'Authorization':'Bearer'+token,
            },
            method:'POST',
        })
    },
    getAll({user_id}){
        let params=null;
        if (user_id){
            params="?user_id="+user_id;
        }
        return fetch(Config.apiPath+'/blogs'+params,{
                headers: {
                'Accept': 'application/json',
              },
              method: "GET",
              
        });
    },
    getOne(blogPath){
        return fetch(Config.apiPath+'/blogs/'+blogPath,{
                headers: {
                'Accept': 'application/json',
              },
              method: "GET",
              
        });
    },
    getOneById(blogId){
        
        return fetch(Config.apiPath+'/blogs/id/'+blogId,{
            headers: {
            'Accept': 'application/json',
          },
          method: "GET",
          
        });

    },

    updateBlog(blogId,blogData,token){


        return fetch(Config.apiPath+'/blogs/'+blogId,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token,
              },
              method: "PUT",
              body: JSON.stringify(blogData)
        });
    },
    deleteBlog(blogId,token){


        return fetch(Config.apiPath+'/blogs/'+blogId,{
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer '+token,
              },
              method: "DELETE",
             
        });
    },

}