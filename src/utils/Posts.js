import Config from '../config';


export default {
    /**
     * 
     * @param {FormData object} postData 
     * @param {String} token 
     */
    createPost(postData,token){

        console.log(postData.get('desc'));
        return fetch(Config.apiPath+'/posts',{
            headers: {
                'Accept': 'application/json',
               
                'Authorization': 'Bearer '+ token,

              },
              method: "POST",
              body: postData,
        });
    },

    getAll({blog_id},skip='',limit='',search=''){

        let params=null;
        if (blog_id){
            params=`?blog_id=${blog_id}&skip=${skip}&limit=${limit}&search=${search}`;
        }
        console.log(params);
        return fetch(Config.apiPath+'/posts'+params,{
                headers: {
                'Accept': 'application/json',
              },
              method: "GET",
              
        });
    },
    getOne(postId){
        return fetch(Config.apiPath+'/posts/'+postId,{
                headers: {
                'Accept': 'application/json',
              },
              method: "GET",
              
        });
    },

    /**
     * 
     * @param {String} postId 
     * @param {FormData} postData 
     * @param {String} token 
     */
    updatePost(postId,postData,token){


        return fetch(Config.apiPath+'/posts/'+postId,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token,
              },
              method: "PUT",
              body: JSON.stringify(postData)
        });
    },

    delete(postId,token){


        return fetch(Config.apiPath+'/posts/'+postId,{
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer '+token,
              },
              method: "DELETE",
             
        });
    },

}