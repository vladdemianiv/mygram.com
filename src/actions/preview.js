export function setPreviewBlog(blog){
    return {
        type:'SET_BLOG',
        blog
    }
}
export function removePreviewBlog(){
    return {
        type:'REMOVE_BLOG',
    }
}