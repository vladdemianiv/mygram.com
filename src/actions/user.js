import usersUtils from '../utils/Users';


export function getUserData(token){
    return (dispatch)=>{
        usersUtils
            .getAuthorizedUser(token)
            .then(responce => {
                return responce.json()
            })
            .then(body => {
                if (body.userData) {
                    const userData=body.userData;
                    dispatch({ type: 'USER_DATA_LOAD_SUCCESS', data:userData });
                }
                else {
                    dispatch({ type: 'USER_DATA_LOAD_FAILED', error: body });
                }
            })
            .catch(error => {
                console.log(error)
            })
    }
}

export function removeUserData(){
    return {
        type:'USER_DATA_REMOVED',
    }

}

export function updateUserData(userId,userData,token){
    return dispatch=>{
        usersUtils
        .updateUser(userId,userData,token)
        .then(response=>{
            if (response.status==200){
                dispatch({type:'USER_DATA_UPDATE_SUCCESS',userData})
            }
            if (response.status>=400){
                dispatch({type:'USER_DATA_UPDATE_FAILED'})
                response.json()
                        .then(error=>{
                            alert(error);
                        })
            }
               
        })

    }

}
