import React from 'react';
import {connect} from 'react-redux';
import './DashBoard.css';

import CircularProgress from '@material-ui/core/CircularProgress';

import BlogList from '../../components/DashBoardComponents/BlogList/BLogList';



class DashBoard extends React.Component{
    constructor(props){
        super(props);
    
    }
    unauthorizedContent(){
        return <p>Log in firstly</p>
    }
    loadingContent(){
        return <div style={{textAlign:'center',marginTop:'30%'}}><CircularProgress/></div>
    }
    mainContent(){
        return <div className='dash-container'>
                     <div className='dash-blog-settings'>
                       <BlogList history={this.props.history} userId={this.props.userData._id}></BlogList>
                    </div>

                </div>
    }
    render(){
        let content;
        
        if (localStorage.getItem('token')){
            content = this.loadingContent();
        }
        else{
            content=this.unauthorizedContent();
        }
        
        if (this.props.userData){
            content=this.mainContent();
        }
        
        if (this.props.dataLoadError){
            content=this.unauthorizedContent();
        }

        return content;
    }

}
const mapStateToProps=(state)=>{
    return{
        userData:state.user.data,
        dataLoadError:state.user.dataLoadError,
    }
}
export default connect(mapStateToProps)(DashBoard);