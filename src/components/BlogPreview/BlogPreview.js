import React from 'react';
import './BlogPreview.css'

import Blog from '../Blog/Blog';
import {connect} from 'react-redux';
import {removePreviewBlog} from '../../actions/preview';
import blogsUtils from '../../utils/Blogs';

class BlogPreview extends React.Component{
    constructor (props){
        super(props);
    }
    saveClickHandler(e) {

        blogsUtils
            .updateBlog(this.props.blog._id, this.props.blog, localStorage.getItem('token'))
            .then(responce => {
                if (responce.status == 200) {
                    this.props.history.push('/blog/'+this.props.blog.path)
                }
                else if (responce.status >= 400) {
                    responce.json()
                        .then(error => {
                            alert(error.message);
                        })
                }
            })


    }
    discardClickHandler(){
        this.props.removePreviewBlog();
        this.props.history.push('/dashboard')
    }


        
    
    render(){
        if (this.props.blog)
        return <React.Fragment>
                    <p>Do you want to 
                        <a className='clickable save' onClick={this.saveClickHandler.bind(this)}>save </a> 
                        or 
                        <a className='clickable discard' onClick={this.discardClickHandler.bind(this)}>discard</a> changes?</p>
                    <Blog blog={this.props.blog}></Blog>
                </React.Fragment>
        else{
            return <p>Loading</p>
        }
    }
}
const mapStateToProps=(state)=>{
    return{
        blog:state.preview.blog,
    }
}

const mapDispatchToProps=(dispatch)=>{
    return{
        removePreviewBlog:()=>{
            dispatch(removePreviewBlog());
        }
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(BlogPreview);