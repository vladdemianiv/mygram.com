import React from 'react';
import {connect} from 'react-redux';
import blogsUtils from '../../utils/Blogs';
import usersUtils from '../../utils/Users';

import BlogList from '../../components/BlogList/BlogList';


export default class UserBlogsList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            blogs:null,
        };
        this.loadBlogs(props);
    }
   
    loadBlogs(props){
        const username=props.match.params.username;
        usersUtils.getOne(username)
            .then(response=>{
                if (response.status==200){
                    response.json()
                            .then(user=>{
                                blogsUtils.getAll({user_id:user._id})
                                .then(response=>{
                                    if(response.status==200){
                                        response.json()
                                                .then(blogs=>{
                                                    this.setState({blogs});
                                                })
                                    }
                                    else if(response.status>=400){
                                        alert(response.statusText);
                                    }
                                })

                            })
                }
                else if (response.status>=400){
                    response.json()
                            .then(error=>{
                                alert(error.message);
                                this.setState({error:error.message});
                            })
                }
            })
            
        
    }
    loadingPageContent(){
        return <p>Loading...</p>
    }
    render (){
       if (this.state.error){
           return <p>{this.state.error}</p>
       }
        
            if(!this.state.blogs){
                
                return this.loadingPageContent();
            }
            else {
                return  <BlogList blogs={this.state.blogs}/>;
            }

        
       
        
    }
}
