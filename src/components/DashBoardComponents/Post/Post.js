import React from 'react';
import './Post.css';
import postsUtils from '../../../utils/Posts';
import CircularProgress from '@material-ui/core/CircularProgress';

/**
 * @props post
 * @props reloadParent
 */
export default  class Post extends React.Component{
    constructor (props){
        super(props);
        this.state={
            post:props.post,
            prevPost:{...props.post},
            loading:false,
            status:'up-to-date',
        } 
      

    }


    
    descChangeHandler(e){
        this.setState({
            post:{
                ...this.state.post,
                desc:e.target.value,      
                }
            }
        );

        if (this.state.prevPost.desc!=e.target.value){
            this.setState({
                status:'modified',
            })
        }
        else {
            this.setState({
                status:'up-to-date',
            })
        }
    }
    updateClickHandler(e){
        if (this.descInput.value==''){
            this.setState({status:'error'});
            return;
        }
        const Data={desc:this.descInput.value};
        
        this.setState({
            loading:true,
        })
        postsUtils
            .updatePost(this.state.post._id,Data,localStorage.getItem('token'))
            .then(responce=>{
                if(responce.status==200){

                    this.props.updateParent();
                    this.setState({loading:false,
                                    status:'up-to-date'});
                   
                }
                if(responce.status>=400){
                    this.setState({status:'error'});
                    this.setState({loading:false});
                }
            })
    }
    loadingPageContent(){
        return <div ><CircularProgress/></div>
    }
    deleteClickHandler(){
        postsUtils
            .delete(this.state.post._id,localStorage.getItem('token'))
                .then(responce=>{
                   console.log(responce);
                   this.props.updateParent();
                })
    }
    render(){
        if (this.state.loading){
            return this.loadingPageContent();
        }
        
        return  <div className={"dash-post-container"+" "+this.state.status}>
                    <div className='dash-post-image'>
                        <img src={this.state.post.image_url} alt='post'></img>
                    </div>
                    <div className='dash-post-form'>
                        <form ref={form=>{this.formNode=form}} >
                           <textarea rows ='4' name='desc' ref={node=>{this.descInput=node}}onChange={this.descChangeHandler.bind(this)} value={this.state.post.desc}></textarea>                          
                        </form>
                    </div>
                    
                    <button className='update-btn' disabled={this.state.status!='modified'} onClick={this.updateClickHandler.bind(this)}>Update</button>
                    <button className='delete-btn' onClick={this.deleteClickHandler.bind(this)}>Delete</button>
                    
                    
                </div>
    }

}

