import React from 'react';
import './Blog.css';
import PostsList from '../PostsList/PostsList';
import blogsUtils from '../../../utils/Blogs';

import CircularProgress from '@material-ui/core/CircularProgress';
import dropdownIcon from '../../../icons/dropdown.png';
import deleteIcon from '../../../icons/delete.png';
import {connect} from 'react-redux';
import {setPreviewBlog} from '../../../actions/preview';


const PATH_MATCH=/^[a-z_]+$/i
/**
 * @props blog
 * @props reloadParent
 */
 class Blog extends React.Component{
    constructor (props){
        super(props);
        this.state={
            blog:props.blog,
            blogModel:{...props.blog},
            
            loading:false,
            status:'up-to-date',
            postsShown:false,
        }
      
           
        
       

    }
    
   

    loadingPageContent(){
        return <div ><CircularProgress/></div>
    }
    dropdownClickHandler(e){
        e.preventDefault();
        this.setState({postsShown:!this.state.postsShown});
    }
 
    nameChangeHandler(e){
        this.setState({
            blogModel:{
                ...this.state.blogModel,
                name:e.target.value,
            }
        })
        if (e.target.value!=this.state.blog.name){
            e.target.style.borderBottom='2px solid yellow';
        }
        else {
            e.target.style.border='none';
        }
    }
    pathChangeHandler(e){
        this.setState({
            blogModel:{
                ...this.state.blogModel,
                path:e.target.value.toLowerCase(),
            }
        }) 
       
        if (e.target.value!=this.state.blog.path){
            e.target.style.borderBottom='2px solid yellow';
        }
        else {
            e.target.style.border='none';
        }
        if (!e.target.value.match(PATH_MATCH)){
            e.target.style.borderBottom='2px solid red';
        }
    }
    themeChangeHandler(e){
        this.setState({
            blogModel:{
                ...this.state.blogModel,
                theme_id:e.target.value,
            }
        })
        if (e.target.value!=this.state.blog.theme_id){
            e.target.style.borderBottom='2px solid yellow';
        }
        else {
            e.target.style.border='none';
        }
    }
    somethingChanged(){
        if (this.state.blogModel.path != this.state.blog.path ||
            this.state.blogModel.name != this.state.blog.name ||
            this.state.blogModel.theme_id != this.state.blog.theme_id) {
            return true;
        }
        else {
            return false;
        }
    }

    saveClickHandler(e){
        e.preventDefault();
        if (this.somethingChanged()){
            let errorFlag=false;
            if (this.state.blogModel.name==''){
                this.nameNode.style.borderBottom='2px solid red';
                errorFlag=true;
            }
            else {
                this.nameNode.style.borderBottom='none';
            }
            if (this.state.blogModel.path==''||!this.state.blogModel.path.match(PATH_MATCH)){
                this.pathNode.style.borderBottom='2px solid red';
                errorFlag=true;
                
            }
            else{
                this.pathNode.style.borderBottom='none';
            }
            
            if (errorFlag) return;
            else {
                let blogData={
                    name:this.state.blogModel.name,
                    path:this.state.blogModel.path.toLowerCase(),
                    theme_id:this.state.blogModel.theme_id,
                }

                blogsUtils
                    .updateBlog(this.state.blog._id,blogData,localStorage.getItem('token'))
                    .then(responce=>{
                        if (responce.status==200){
                            this.props.reloadParent();
                            this.setState({loading:false,
                                status:'up-to-date',});
                        }
                        else if (responce.status>=400){
                            responce.json()
                                    .then(error=>{
                                        alert(error.message);
                                    })
                        }
                    })


            }
            this.themeNode.style.border='none';


        }
    }

    deleteClickHandler(e){
        
        e.preventDefault();
        blogsUtils.deleteBlog(this.state.blog._id,localStorage.getItem('token'))
                  .then(responce=>{
                    if (responce.status==200){
                        this.props.reloadParent();
                    }
                    else if (responce.status>=400){
                        responce.json()
                                .then(error=>{
                                    alert(error.message);
                                })
                    }
                  })
     
    }
    previewClickHandler(e){
        e.preventDefault();
        if (this.somethingChanged()){
            
            this.props.setPreviewBlog(this.state.blogModel);
            this.props.history.push('/preview#'+this.state.blogModel.path);
        }
    }
    render(){
        if (this.state.loading){
            return this.loadingPageContent();
        }
        let postslist=null;
        if (this.state.postsShown){
            postslist=<PostsList blogId={this.state.blog._id}></PostsList>
        }
        
        return  <div >
                    <form className={"dash-blog-container"+" "+this.state.status}>
                        <input ref={node=>{this.nameNode=node}}type='text' placeholder='Name' value={this.state.blogModel.name} 
                        onChange={this.nameChangeHandler.bind(this)
                        }></input>
                        <input ref={node=>{this.pathNode=node}}className='blog-path-input' type='text'placeholder='Blog url' value={this.state.blogModel.path}
                        onChange={
                            this.pathChangeHandler.bind(this)
                        }
                        ></input>
                        <select ref={node=>{this.themeNode=node}}value={this.state.blogModel.theme_id}
                        onChange={this.themeChangeHandler.bind(this)}
                        >
                            <option value='0'>Default</option>
                            <option value='1'>Blue</option>
                            <option value='2'>Dark</option>
                        </select>
                        <button className='preview-btn' onClick={this.previewClickHandler.bind(this)}>Preview</button>
                        <button className='save-btn' onClick={this.saveClickHandler.bind(this)}> Save</button>
                        <button className='posts-btn' onClick={this.dropdownClickHandler.bind(this)}> <img style={this.state.postsShown?{transform:'rotate(180deg)'}:{}}src={dropdownIcon}></img></button>
                        <button className='delete-btn' onClick={this.deleteClickHandler.bind(this)}><img src={deleteIcon}></img></button>     
                    </form>
                    
                    <div className='dash-blog-postlist'>
                        {postslist}
                    </div>
                    
                </div>
                
    }

}
const mapDispatchToProps=(dispatch)=>{
    return{
        setPreviewBlog:(blog)=>{
            dispatch(setPreviewBlog(blog));
        }
    }

}

export default connect(null,mapDispatchToProps)(Blog)