import React from 'react';
import './PostsList.css';
import Post from '../Post/Post';
import postsUtils from '../../../utils/Posts';
import CircularProgress from '@material-ui/core/CircularProgress';


/**
 * @props blogId
 */
export default  class PostsList extends React.Component{
    constructor (props){
        super(props);
        this.state={
            posts:null,            
            loading:true,
            imageLoaded:false,
            addButtonActive:false,     
        }
        
        this.loadPosts();

    }
    loadPosts(){
        this.setState({
            loading:true,
        });
        postsUtils
            .getAll({blog_id:this.props.blogId})
            .then(responce=>{
                if (responce.status==200){
                    responce
                        .json()
                        .then(posts => {
                            this.setState({
                                posts,
                                loading: false,
                            })
                        })
                }
                else{
                    this.setState({
                        error:responce.statusText,
                    })
                }
            })
    }
    loadingPageContent(){
        return <div ><CircularProgress/></div>
    }
    imageChangeHandler(e){
        this.setState({
            imageLoaded:true,
        })
        if (this.desc.value!=''){
            this.setState({addButtonActive:true});
        }
        else{
            
            this.setState({addButtonActive:false});
        }
        
    }
    addPostClickHandler(e){
        e.preventDefault();
        if (this.state.imageLoaded&&this.desc.value){
            this.setState({
                loading:true,
            });
            const data=new FormData(this.form);
            data.set('blog_id',this.props.blogId);
            
            postsUtils
                .createPost(data,localStorage.getItem('token'))
                .then(responce=>{
                    if (responce.status==201){
                        this.loadPosts();
                        this.desc.value='';
                        this.form.reset();
                        this.setState({
                            imageLoaded:false,
                            addButtonActive:false,
                        })
                    }
                    if(responce.status>=400){
                        responce.json()
                            .then(body=>{
                                console.log(body.error);
                            })
                        
                    }
                })
                .catch(error=>{
                    console.log(error);
                })
        }
        
    }
    descChangeHandler(e){
        if (e.target.value!=''&&this.state.imageLoaded){
            this.setState({addButtonActive:true});
        }
        else {
            this.setState({addButtonActive:false});
        }
    }

    
    render(){
       
        
        if (this.state.posts){
            const posts=this.state.posts.map(post=>{
            return <Post key={post._id} updateParent={this.loadPosts.bind(this)} post={post}></Post>
             })
            return <div className='dash-postslist-container'>
                        <div className='dash-postslist-addpost'>
                            <form ref={node=>this.form=node}>
                            <div className="upload-btn-wrapper">
                            <button ref={node => { this.imageBtn = node }} className="btn" style={
                                this.state.imageLoaded ? {
                                    borderColor: '#4CAF50',
                                    color: '#4CAF50'
                                } : {}
                            }>{
                                    this.state.imageLoaded ? 'Image loaded' :
                                        'Upload image'}
                            </button>
                                <input ref={node=>this.fileInput=node}type="file" name="image" onChange={this.imageChangeHandler.bind(this)}/>
                            </div>
                                <textarea ref={node=>this.desc=node}name='desc' placeholder='Enter post description' onChange={this.descChangeHandler.bind(this)}></textarea> 
                           
                            <button className={`dash-postlist-add-btn ${this.state.addButtonActive?'active':''}`} onClick={this.addPostClickHandler.bind(this)}>Add</button>
                            </form>
                        </div>
                        <div class='dash-postslist-posts'>
                            {posts}
                        </div>
            </div>
        }
        else return this.loadingPageContent();
    }

}