import React from 'react';
import './BlogList.css';
import Blog from '../Blog/Blog';
import blogsUtils from '../../../utils/Blogs';
import CircularProgress from '@material-ui/core/CircularProgress';

const PATH_MATCH=/^[a-z_]+$/i;
/**
 * @props userId
 */
export default class BlogList extends React.Component{
    constructor(props){
        super(props);
        this.state={
            blogs:null,
            loading:true,
        }
        this.loadBlogs();
    }

    loadBlogs(){
        //this.setState({loading:true});
        blogsUtils
            .getAll({user_id:this.props.userId})
            .then(response=>{
                if (response.status==200){
                    response
                        .json()
                        .then(blogs=>{
                            this.setState({
                                    blogs,
                                    loading:false,
                                });
                            })
                }
                else if (response.status>=400){
                   response.json()
                            .then(error=>{
                                this.setState({error});
                            })
                }
            })

    }

    loadingPageContent(){
        return <div ><CircularProgress/></div>
    }

    newBlogClickHandler(e){
        e.preventDefault();
        if(this.newName.value==''){
            this.newName.style.borderBottom='2px solid red';
        }
        else{
            this.newName.style.borderBottom='0';
        }
        if(this.newPath.value==''||!this.newPath.value.match(PATH_MATCH)){
            this.newPath.style.borderBottom='2px solid red';
        }
        else{
            this.newPath.style.borderBottom='0';
        }
        if(this.newPath.value!=''&&this.newName.value!=''&&this.newPath.value.match(PATH_MATCH)){
            const blogData={
                name:this.newName.value,
                path:this.newPath.value.toLowerCase(),
                theme_id:this.newTheme.value,
                user_id:this.props.userId,

            }
            blogsUtils
                .createBlog(blogData,localStorage.getItem('token'))
                .then(response=>{
                    if(response.status==201){
                        this.loadBlogs();
                        this.newPath.value='';
                        this.newName.value='';
                        this.newTheme.value='0';
                    }
                    if(response.status>=400){
                        response.json()
                                .then(error=>{
                                    alert(error.message);
                                })
                    }
                })
        }

    }
    render(){

        if (this.state.blogs){
            const blogs=this.state.blogs.map(blog=>{
                return <Blog  history={this.props.history}key={blog._id} reloadParent={this.loadBlogs.bind(this)}blog={blog}></Blog>
            });

            return <div className='dash-bloglist-container'>
                        <h1 className='create-blog'>Create blog</h1>
                        <div className='dash-bloglist-addblog'>
                            <form className='add-blog-form'>
                            <input ref={node=>{this.newName=node}} type='text' placeholder='Name' ></input>
                            <input ref={node=>{this.newPath=node}}type='text' placeholder='Blog url' style={{textTransform:'lowercase'}}></input>
                            <select ref={node=>{this.newTheme=node}} >
                                <option value='0'>Default theme</option>
                                <option value='1'>Blue theme</option>
                                <option value='2'>Dark theme</option>
                            </select>
                            <button className='add-btn' onClick={this.newBlogClickHandler.bind(this)}>New blog</button>
                            </form>
                        </div>
                        <h1 >Blogs</h1>
                        <div className='dash-bloglist-blogs'>
                            <tr className='table-head'>
                                <th>Name</th>
                                <th>Path</th>
                                <th>Theme</th>
                                <th></th>
                            </tr>
                            {blogs}
                        </div>
                    </div>
        }
        else {
            return this.loadingPageContent();
        }
    }
}