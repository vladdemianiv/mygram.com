import React from 'react';
import {connect} from 'react-redux';

import './Account.css';
import settingsImage from '../../icons/settings.png';
//import '../../Sidebar/Sidebar.css';
import {Link } from 'react-router-dom';

import {removeUserData} from '../../actions/user';
class Account extends React.Component{
    

    constructor(props){
        super(props);
      
        

    }
    handleClick(e){
        
        const arrow=document.getElementById('account-arrow');
        const content=document.getElementById('account-content');
        arrow.classList.toggle('up');
        arrow.classList.toggle('down');
        content.classList.toggle('displayed');
        
    }
    handleLogoutClick(e){
        this.props.logout();
        
    }


  
    render(){
        

        let content ;
        
        if (this.props.authorized){
                    content= <div className='user-info '>
                            <span class='full-name'>{`${this.props.user.firstname} ${this.props.user.lastname}`}</span>
                            <Link to='/settings'><img className='settings-icon' alt='settings' src={settingsImage} /></Link>
                            <span class='username'>{this.props.user.username}</span>
                            <Link to="/dashboard" className='menu-item sub' onClick={this.props.onClick}>Blogs dashboard</Link>
                            <Link to={`/${this.props.user.username}/blogs`} className='menu-item sub' onClick={this.props.onClick}>My blogs</Link>
                            <Link to='/'className='menu-item sub last log-out' onClick={this.handleLogoutClick.bind(this)}>Log out</Link>
                        </div>;
        } 
        else {
                content=  <div className='user-info'>

                            <Link to='/login' className='menu-item sub' onClick={this.props.onClick}>Log in</Link>
                            <Link to='/signup' className='menu-item sub last' onClick={this.props.onClick}>Sign up</Link>
                        </div>;
        }


        return <div id={this.props.id} className={this.props.className}>
                    
                        <div onClick={this.handleClick} className='clicking-zone'>
                            <a >
                            Account<i id='account-arrow' className='arrow up'></i>
                            </a>
                        </div>

                        <div id="account-content" className={"account-content displayed"}>{content}</div>
                    
                </div>
    }

}

const mapStateToProps = (state)=>{
    return{
        authorized:state.user.data?true:false,
        user:state.user.data,
    }
};

const mapDispatchToProps =(dispatch)=>{
    return{
        logout:()=>{
            localStorage.removeItem('token');
            return dispatch(removeUserData());
        }
    };
}
export default connect(mapStateToProps,mapDispatchToProps)(Account);