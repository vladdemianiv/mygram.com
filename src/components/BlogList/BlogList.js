import React from 'react';
import './BlogList.css';
import BlogListItem from '../BlogListItem/BlogListItem';

/** @props blogs */
export default class BlogList extends React.Component{

 
    mapIdToTheme(id){
        const map=[
            'theme-default',
            'theme-blue',
            'theme-dark'
        ]
        return map[id];

    }
    render(){
        let blogs;
        if (this.props.blogs) {
            if(this.props.blogs.length==0){
                blogs=<h2 style={{padding:'100px',textAlign:'center'}}>No blogs yet</h2>
            }
            else{
                blogs = this.props.blogs.map(blog => {
                    return <BlogListItem blog={blog}/>
                })
            }
        }
         
         return <ul className='list'>
                   {blogs}
                </ul>;
    }
}


