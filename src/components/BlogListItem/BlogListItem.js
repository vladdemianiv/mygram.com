import React from 'react';

import {Link} from 'react-router-dom';
import '../BlogList/BlogList.css';
function formatDate(date){
    return `${date.getHours()}:${date.getMinutes()}  ${date.getDate()}.${date.getMonth()+1}.${date.getFullYear()}`;
}
/** @props blog */
export default class BlogListItem extends React.Component{

 
    mapIdToTheme(id){
        const map=[
            'theme-default',
            'theme-blue',
            'theme-dark'
        ]
        return map[id];

    }
    render(){
        return <li className={`blog ${this.mapIdToTheme(this.props.blog.theme_id)}`}>
                    <Link to={`/blog/${this.props.blog.path}`} className='blog-name' style={{color:''}}>{this.props.blog.name}</Link>
                    <p className='owner'>By {this.props.blog.user_id.username}<br/>{formatDate(new Date(this.props.blog.created_at))}</p>
                </li>    
    }
}