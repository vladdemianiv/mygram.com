import React from 'react';
import './Post.css';
import '../Blog/Blog.css';

export class Post extends React.Component{
    render(){
        return <div id={this.props.id} className={this.props.className}>
                    <div className="post-wrapper">
                        <img src={this.props.imagePath} className="post-img" alt="Post"/>
                        <p className="post-desc">{this.props.desc} </p>
                    </div>
                </div>
    }
}
