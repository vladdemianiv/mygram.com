import React from 'react';
import './Form.css';
//import {signup as signupAction}  from '../../actions/signup';
import usersUtils from '../../utils/Users';
import {connect} from 'react-redux';


const EMAIL_MATCH=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 
class SignUp extends React.Component{
    constructor(props){
        super(props);
        this.state={
            result:null,
        }
    }
    validateForm(){
       
       
        if (this.username.value.length==0||
            this.firstname.value.length==0||
            this.lastname.value.length==0||
            this.email.value.length==0||
            this.password.value.length==0||
            this.passwordConfirm.value.length==0){
                this.errorField.style.display='inline';
                this.errorField.style.color='red';
                this.errorField.innerText="Fill in all the fields";
                setTimeout(() => {
                    this.errorField.style.color='gray';
                }, 200);
                return false;
        }
        if (!this.email.value.match(EMAIL_MATCH)){
            this.errorField.style.display='inline'
            this.errorField.style.color='red';
            this.errorField.innerText="Enter valid email";
            setTimeout(() => {
                this.errorField.style.color='gray';
            }, 200);
            return false;
        }
        if (this.password.value!==this.passwordConfirm.value){
            this.errorField.style.display='inline';
            this.errorField.style.color='red';
            this.errorField.innerText="Passwords do not match ech other";
            setTimeout(() => {
                this.errorField.style.color='gray';
            }, 200);
            return false;
        }
        this.errorField.style.display='none';
        return true;
        
    }
    
    submitClickHandler(){
    if (this.validateForm()){
            const userData={
                username:this.username.value,
                firstname:this.firstname.value,
                lastname:this.lastname.value,
                email:this.email.value,
                password:this.password.value
            }
            usersUtils 
                .performSingUp(userData)
                .then(response=>{
                    if(response.status==201){
                        this.setState({result:'Registration successful.'})
                    }
                    else if(response.status>=400){
                        response.json() 
                                .then(error=>{
                                    this.setState({result:'Error! '+error.message});
                                })
                    }
                })

            
            
        }
    }
    render(){
        if(this.props.authorized){
            return <p className='msg-error'>Log out firstly.</p>
        }
        if (this.state.result){
          if (this.state.result.startsWith('Error')){
            setTimeout(()=>{
                this.props.history.push('/signup')
            },2000);
            return <p className='msg-success'>{this.state.result}</p>
          }
          else{
            setTimeout(()=>{
                this.props.history.push('/login')
            },2000);
            return <p className='msg-error'>{this.state.result}</p>
          }
        }
      
        return <div  className={this.props.className}>
                   <div className='form' >
                        <h2>Sign Up</h2>
                        <span>Username</span>
                        <input type='text' ref={(node)=>{this.username=node}} placeholder='Username'></input>
                        <span>First name</span>
                        <input type='text' ref={(node)=>{this.firstname=node}} placeholder='First name'></input>
                        <span>Last name</span>
                        <input type='text' ref={(node)=>{this.lastname=node}} placeholder='Last name'></input>
                        <span>Email</span>
                        <input type='email' ref={(node)=>{this.email=node}} placeholder='Email'></input>
                        <span>Password</span>

                        <input type='password' ref={(node)=>{this.password=node}} placeholder='Password'></input>
                        <input type='password' ref={(node)=>{this.passwordConfirm=node}}placeholder='Confirm password'></input>
                        <span ref={node=>{this.errorField=node}} style={{color:'red',display:'none'}}></span><br/>
                        <input type='submit' value='Submit' onClick={this.submitClickHandler.bind(this)}></input>
                        
                   </div>
                </div>
    }
};

const mapStateToProps=(state)=>{

   return  {
       authorized:state.user.data?true:false,
    }

};

export default connect(mapStateToProps)(SignUp); 