import React from 'react';
import './Form.css';
import {connect} from 'react-redux';
import {getUserData} from '../../actions/user';
import usersUtils from '../../utils/Users';


class Login extends React.Component{
    constructor(props){
        super(props);
        this.state={
            error:null,     
        }
        
    }
    validateForm(){
        this.loginData=new FormData(this.formNode);
        if (this.loginData.get('login').length==0||this.loginData.get('password').length==0){
            this.errorField.style.display='inline';
                this.errorField.style.color='red';
                this.errorField.innerText="Fill in all the fields";
                setTimeout(() => {
                    this.errorField.style.color='gray';
                }, 200);
                return false;
        }
        return true;

    }
    

    submitClickHandler(e){
        e.preventDefault();
        const loginFormData=new FormData(this.formNode);
        if (this.validateForm()){
            const loginData={};
           
            loginFormData.forEach(function(value, key){
                loginData[key] = value;
            });

            usersUtils
                .performLogin(loginData)
                .then(response=>{  
                    if (response.status==200){
                        response.json()
                                .then(body=>{
                                    localStorage.setItem('token',body.token);
                                    this.props.getUserByToken(body.token);
                                   
                                })
                    }
                    else if (response.status>=400){
                        response.json()
                                .then(error=>{
                                   
                                    this.setState({error:'Error! '+error.message});
                                })
                    }
                    
                })
        }
    }
    
    render(){
        if (this.state.error){
            return <p className='msg-error'>{this.state.error}</p>
        }
        if (this.props.authorized){
            this.props.history.push('/dashboard');
        }

        return <div id={this.props.id} className={this.props.className}>
                   <form className='form' 
                    ref={(node)=>{
                       this.formNode=node;
                   }}

                    onSubmit={this.submitClickHandler.bind(this)}
                   >
                        <h2>Log in</h2>
                        
                        <input type='text' name='login' placeholder='Username/Email'></input>
                       
                        <input type='password' name='password' placeholder='Password'></input>
                        <span ref={node=>{this.errorField=node}} style={{color:'red',display:'none'}}></span><br/>
                      
                        <input type='submit' value='Submit'></input>
                   </form>
                   
                </div>
    }
}

const mapStateToProps=(state)=>{
    return {
        authorized:state.user.data?true:false,
    };
};
const mapDispatchToProps=(dispatch)=>{
    return{
        getUserByToken:(token)=>{
            dispatch(getUserData(token));
        }
    }

}

export default connect (mapStateToProps,mapDispatchToProps)(Login);