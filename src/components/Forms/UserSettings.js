import React from 'react';
import './Form.css';
import {connect} from 'react-redux';
import {updateUserData }from '../../actions/user';
import usersUtils from '../../utils/Users';

const EMAIL_MATCH =/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
 class UserSettings extends React.Component{

    constructor(props) {
        super(props);
        this.state={

        }
    }
    onInputChange(){
        if (this.username.value!=''||
            this.firstname.value!=''||
            this.lastname.value!=''||
            this.email.value!=''){
                this.updateNode.disabled=false;
            }
            else {
                this.updateNode.disabled=true;
            }
    }
    submitUpdate(e){

        if ( this.username.value==''&&
            this.firstname.value==''&&
            this.lastname.value==''&&
            this.email.value==''){
                return;
            }
      
        const userData={}
        if (this.username.value!=''){
            userData.username=this.username.value;
        }
        if (this.firstname.value!=''){
            userData.firstname=this.firstname.value;
        }
        if (this.lastname.value!=''){
            userData.lastname=this.lastname.value;
        }
        
        if (this.email.value!=''){
            if (this.email.value.match(EMAIL_MATCH)){
                userData.email=this.email.value;
                this.email.style.borderBottom='1px solid #ddd';
            }
            else{
                this.email.style.borderBottom='2px solid red';
                return;
            }
        }
        
        this.props.updateUser(this.props.userData._id,userData,localStorage.getItem('token'));
        
        
        this.username.value='';
        this.firstname.value='';
        this.lastname.value='';
        this.email.value='';
    }

    validatePasswordChange(){
        if (this.oldPass.value==''){
            this.oldPass.style.borderBottom='2px solid red';
            setTimeout(()=>{
                this.oldPass.style.borderBottom='2px solid #ddd';
            },2000);
        }
        if (this.newPass.value==''){
            this.newPass.style.borderBottom='2px solid red';
            setTimeout(()=>{
                this.newPass.style.borderBottom='2px solid #ddd';
            },2000);
        }
        if (this.newPassConfirm.value==''){
            this.newPassConfirm.style.borderBottom='2px solid red';
            setTimeout(()=>{
                this.newPassConfirm.style.borderBottom='2px solid #ddd';
            },2000);
        }
        if (this.oldPass.value==''||
            this.newPass.value==''||
            this.newPassConfirm.value=='')
            {
                return false;
            }
        
        if (this.newPass.value!=this.newPassConfirm.value){
            alert ("Passwords don't match each other");
            return false;
        }
        return true;
    }
    submitChangePassword(e){
        if (this.validatePasswordChange()){
            
            const userData={
                password:this.newPass.value
            }

            usersUtils
                .changePassword(this.newPass.value,this.oldPass.value,localStorage.getItem('token'))
                .then(response=>{
                    if (response.status==200){
                        this.oldPass.value='';
                        this.newPass.value='';
                        this.newPassConfirm.value='';
                        alert ("Password changed");
                    }
                    if (response.status>=400){
                        response.json()
                                .then(error=>{
                                    alert(error.message);
                                })

                                
                    }
                })
                .catch(err=>{
                    console.log(err);
                })    

           
        }
            
    }
    backButtonClick(){
        this.props.history.goBack();
    }
    
    render(){
        
        if (!this.props.userData){
            return <p>Firstly Log in</p>
        }

        return <div id={this.props.id} className={this.props.className}>
                   <div className='form' >
                        <h2>Account settings</h2>
                        <span>Username : {this.props.userData.username}</span>
                        <input ref={node=>{this.username=node}}type='text' id='username' placeholder='Username' onChange={this.onInputChange.bind(this)}></input>
                        <span>First name : {this.props.userData.firstname}</span>
                        <input ref={node=>{this.firstname=node}}type='text' id='firstname' placeholder='First name' onChange={this.onInputChange.bind(this)}></input>
                        <span>Last name : {this.props.userData.lastname}</span>
                        <input ref={node=>{this.lastname=node}}type='text' id='lastname' placeholder='Last name' onChange={this.onInputChange.bind(this)}></input>
                        <span>Email : {this.props.userData.email}</span>
                        <input ref={node=>{this.email=node}}type='email' id='email' placeholder='Email' onChange={this.onInputChange.bind(this)}></input>
                        <input ref={node=>{this.updateNode=node}}type='submit' value='Update'  onClick={this.submitUpdate.bind(this)} ></input>
                        <br/>
                        <span>Change password</span>
                        
                        <input ref={node=>{this.oldPass=node}} type='password' placeholder='Old password'></input>
                        <input ref={node=>{this.newPass=node}} type='password' placeholder='New password'></input>
                        <input ref={node=>{this.newPassConfirm=node}} type='password' placeholder='Confirm new password'></input>

                        <input type='submit' value='Submit' onClick={this.submitChangePassword.bind(this)}></input>
                        <input type='submit' value='Back' onClick={this.backButtonClick.bind(this)}></input>
                        
                   </div>
                </div>
    }
}

function mapStateToProps(state){
 
    return {
        userData:state.user.data,
    }
}
function mapDispatchToProps(dispatch){
    return {
        updateUser:(userId,userData,token)=>{
            dispatch(updateUserData(userId,userData,token));
        }
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(UserSettings);