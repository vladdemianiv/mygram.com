import React from 'react';
import './UsersList.css';
import usersUtils from '../../utils/Users';
import {Link} from 'react-router-dom';


export default class BlogList extends React.Component{
    constructor(props){
        super(props);
        this.state={users:null};

        this.getUsers();

    }
    
    getUsers(){
        usersUtils  
            .getAll()
            .then(response=>{
                if (response.status==200){
                    response.json()
                            .then(users=>{
                                this.setState({users:users.sort((a,b)=>{
                                    if (a.firstname.toLowerCase()>b.firstname.toLowerCase()){
                                        return 1;
                                    }
                                    else if (a.firstname.toLowerCase()<b.firstname.toLowerCase()){
                                        return -1;
                                    }
                                    else {
                                        return 0;
                                    }
                            
                                })});
                            });
                    
                }
                else if (response.status>=400){
                    response.json()
                            .then(error=>{
                                alert(error.message);
                            })
                }
            })
            .catch(error=>{
                console.log(error);
            })
    }
 
    render(){
        let users;
        if (this.state.users) {
            
            users=this.state.users.map(user=>{
                return <li className='user'><Link style ={{color:'orangered'}}to={`/${user.username}/blogs`}>{user.firstname+' '+user.lastname}</Link></li>
            })

            if(this.state.users.length==0){
                return <h2 style={{textAlign:'center',padding:'100px'}}>No users</h2>
            }
            
        }
         
         return <ul className='list'>
                   {users}
                </ul>;
    }
}


