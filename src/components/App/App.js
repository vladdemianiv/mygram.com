import React from 'react';
import  Root  from '../Root/Root';

import DashBoard from '../DashBoard/DashBoard';

import SignUp from '../Forms/SignUp'; 
import Login from '../Forms/Login';
import UserBlogsList from '../../components/UserBlogsList/UserBlogsList';
import BlogPreview from '../../components/BlogPreview/BlogPreview';
import Home from '../../components/Home/Home';
import UserSettings from '../../components/Forms/UserSettings';
import UsersList from '../UsersList/UsersList';



import {connect} from 'react-redux';

import {getUserData} from '../../actions/user'

import {BrowserRouter as Router,Route} from 'react-router-dom';
import  Blog  from '../../components/Blog/Blog';





  class App extends React.Component{
    constructor(props){
        super(props)
        let token=null;
        token=localStorage.getItem('token');
        if (token)
        this.props.getUserByToken(token);
    }
    render(){
        return <Router > 
                  
                    <Root>
                        <Route exact path='/' component={Home}/>
                        <Route exact path='/signup' component={SignUp}/>
                        <Route exact path='/login' component={Login}/>    
                        <Route exact path='/:username/blogs' component={UserBlogsList}/>
                        <Route exact path='/settings' component= {UserSettings}/>    
                        <Route exact path='/blog/:blogPath' component = {Blog}/> 
                        <Route exact path='/dashboard' component={DashBoard}/> 
                        <Route path ='/preview' component={BlogPreview}/>
                        <Route path ='/users' component={UsersList}/>
                        
                       
                     </Root>
                    
                    
                </Router>
                  
                
    }
}

const mapDispatchToProps=(dispatch)=>{
    return{
        getUserByToken:(token)=>{
            dispatch(getUserData(token));
        }
    }

}
export default connect(null,mapDispatchToProps)(App);
