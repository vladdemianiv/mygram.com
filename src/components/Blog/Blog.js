import React from 'react';
import './Blog.css';
import {Post} from '../Post/Post';
import {Link} from 'react-router-dom';
import blogsUtils from '../../utils/Blogs';

import postsUtils from '../../utils/Posts';
import color from '@material-ui/core/colors/teal';

 
export default class Blog extends React.Component{

    constructor(props){
        super(props);
        
        
       
        if (this.props.match){
        
            this.state={
                blog:null,
                posts:null,
                searchWord:'',
            }
            this.loadData(this.props.match.params.blogPath);

        }
        else if (props.blog){
            console.log('yes');
            this.state={
                blog:props.blog,
                posts:null,
                searchWord:'',
            };
            this.loadPosts(props.blog);
        }
    }
    loadData(path){
        blogsUtils.getOne(path)
        .then(response=>{
            if (response.status==200){
                return response.json()
            }
            if (response.status>=400){
                response.json().then(error=>{
                    alert(error.message);
                })
            }

        })
        .then(blog => {
            this.setState({blog});
            this.loadPosts(blog);
        })
        .catch(error=>{
            console.log(error);
        })
    }
    loadPosts(blog){
        this.setState({allPostsLoaded:false})
        
        postsUtils.getAll({ blog_id:blog._id},0,12,this.state.searchWord)
        .then(responce => {
            return responce.json()
        })
        .then(posts => {
            this.setState({posts});
        })
        .catch(error => {
            console.log(error)
        })
    }
    loadMorePosts(){
        if (this.state.allPostsLoaded){
            return;
        }
        postsUtils.getAll({blog_id:this.state.blog._id},this.state.posts.length,9,this.state.searchWord)
            .then(response=>{
                if (response.status==200){
                    return response.json();
                }
                else{
                    response.json()
                            .then(error=>{alert(error.message)});
                }
            })
            .then(posts=>{
                if (posts.length==0){
                    this.setState({allPostsLoaded:true});
                }
                else{
                    if (posts.length>9){
                         this.setState({allPostsLoaded:true});
                    }
                    this.setState({
                        posts:[...this.state.posts,...posts],
                    });
                }
            })
    }
    mapIdToTheme(id){
        const map=[
            'blog-theme-default',
            'blog-theme-blue',
            'blog-theme-dark'
        ]
        return map[id];

    }
    scrollHandler(e){
        //if (scrolled to the bottom)
        if(this.postsWrapper.scrollTop==this.postsWrapper.scrollHeight-this.postsWrapper.clientHeight){
            this.loadMorePosts();
        }
    }
    onSearchInputTextChange(e){
        this.setState({searchWord:e.target.value},()=>{
            this.loadPosts(this.state.blog);
        });
        
    }
    formatPosts(posts){
        let postsNodes=posts.map(post=><Post className='post' imagePath={post.image_url} desc={post.desc}/>)
        let columns=[[],[],[]];
        postsNodes.forEach((post,index) => {
            columns[index%3].push(post);
        });  
        
        let result= <div ref={node=>this.postsWrapper=node} className="posts-wrapper" onScroll={this.scrollHandler.bind(this)}>
                        <div className="column">      
                        {columns[0]}
                        </div>
                        <div className="column">
                        {columns[1]}
                        </div>
                        <div className="column">
                        {columns[2]}
                        </div>
                </div>
        if (posts.length==0){
            result=<h1 style={{color:(this.state.blog.theme_id==0?'black':'white'),paddingTop:'100px'}}>No posts</h1>
        }
        return result;
    }
    render(){

        let posts;
        if (this.state.posts){
           posts=this.formatPosts(this.state.posts)
        }
        
        
        if (!this.state.blog){
            return <p>Loading...</p>
        }
        if (this.state.blog){
        return  <div className={"blog-wrapper "+this.mapIdToTheme(this.state.blog.theme_id)}>
                      <div className="blog-title">
                        <h1>{this.state.blog.name}</h1>
                        <input className='search-field' type='text' placeholder='Search' value={this.state.searchWord} onChange={this.onSearchInputTextChange.bind(this)}></input>
                      </div>
                      {posts}
                </div>
        }
               
    }
}


