import React from 'react';
import {Link} from 'react-router-dom';
import './Home.css';
import {connect} from 'react-redux';
import usersUtils from '../../utils/Users';

 class Home extends React.Component{

    constructor(props){
        super(props);
        this.state={
            usersCount:null,
        }
        this.getUsersCount();
    }
    getUsersCount (){
        usersUtils.getCount()
            .then(response=>{
                if (response.status==200){
                    response.json()
                            .then(body=>{
                                this.setState({usersCount:body.usersCount});
                            })
                }
            })
    }
    render(){
        let joinus=<h3 className='home-join-us'>{this.state.usersCount?`${this.state.usersCount} using MyGram.`:''} <Link to='/signup' >Join us</Link></h3>;
        if (this.props.authorized){
            joinus='';
        }
        return <div className='home-container'>
                    <h1 className='home-title'>MyGram</h1>
                    <h2 className='home-description'>Just post it</h2>
                    {joinus}
                </div>
    }
}
const mapStateToProps=(state)=>{
    return{
        authorized:state.user.data?true:false
    }
}
export default connect (mapStateToProps)(Home);