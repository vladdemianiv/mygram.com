import React from 'react';

import './Sidebar.css';
//import '../../App/App.css';
import Account from '../Account/Account';
import {Link} from 'react-router-dom';



export class Sidebar extends React.Component{

    setActive(itemId){ 

        const activeItems=Array.prototype.slice.call(document.querySelectorAll('.active'));
        activeItems.forEach(item => {
            item.classList.remove('active');
        });
        document.getElementById(itemId).classList.add('active');
    }

    render(){
        return <div className={this.props.className}> 
                    <nav className='menu'>
                        <Link id="home" to='/' className="menu-item active" onClick={()=>{this.setActive('home')}}>Home</Link>
                        <Account id='account' className="menu-item" onClick={()=>{this.setActive('account')}}></Account>
                        <Link id='users' to='/users'className="menu-item"  onClick={()=>{this.setActive('users')}}>Users</Link>
                    </nav>
                </div>
    }
}
