import { combineReducers } from 'redux';

import user from './userReducer'; 
import preview from './previewReducer';

const reducers = combineReducers({ 
    user,
    preview,
});

export default reducers;