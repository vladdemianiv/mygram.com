const initialState = {
    data:null,
    error:null,
    dataLoadError:null,
    blogsLoadError:null, 
};
 
 export default function user(state = initialState, action) {
   switch (action.type) {
       case 'USER_DATA_LOAD_SUCCESS':
       return {
         ...state,
         data:action.data,

       }
     case 'USER_DATA_LOAD_FAILED':
     return {
         ...state,
         dataLoadError:action.error,
     }
    
     case 'USER_DATA_REMOVED':
     return{
         ...state,
         data:null,
     }
     case 'USER_DATA_UPDATE_SUCCESS':
     return{
       ...state,
       data:{
         ...state.data,
         ...action.userData
       }
     }
     case 'USER_DATA_UPDATE_FAILED':
     return state;
       
     
     default:
       return state;
   }
 }