const initialState = {
    blog:null,
};
 
 export default function preview(state = initialState, action) {
   switch (action.type) {
       case 'SET_BLOG':
       return {
         ...state,
         blog:action.blog

       }
     case 'REMOVE_BLOG':
     return {
         ...state,
         blog:null,
     }
  
     default:
       return state;
   }
 }