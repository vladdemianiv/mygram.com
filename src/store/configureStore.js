import { createStore,applyMiddleware } from 'redux';
import reducers from '../reducers';
import {composeWithDevTools} from 'redux-devtools-extension';

//async actions package
import thunk from 'redux-thunk';

const configureStore = () => {
  return createStore(
    reducers,
    composeWithDevTools(applyMiddleware(thunk)),
  );
};

export default configureStore;