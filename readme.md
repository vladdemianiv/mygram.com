# Project 

mygram.com - is a microblogging website. 
### Prerequisities


In order to run this website you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)


### Running it locally

Firstly you have to create a folder where you'll combine client and server.
```shell
mkdir mygram
```
Then go inside it and clone mygram.com project and API for it.

```shell
cd mygram
git clone https://gitlab.com/vladdemianiv/mygram.com.git
git clone https://gitlab.com/vladdemianiv/api.mygram.svc.com.git
```

Go inside mygram.com folder and run the composition by:
```shell
cd mygram.com
docker-compose up -d
```
To stop composition use
```shell
docker-compose down
```

After configuring local DNS website will be available on mygram.com, while api service will work on api.mygram.svc.com.


# Local DNS setup

Docker composition will run two projects on virtual hosts, see `VIRTUAL_HOST` environment variable in `docker-compose.yml` file:
* api.mygram.svc.com
* mygram.com

To make both domains reachable in your browser you will need to adjust local DNS configuration.
Add these lines to end of your `hosts` file:
```
127.0.0.1   api.mygram.svc.com
127.0.0.1   mygram.com
```

Path to `hosts` file is different:
### For Linux/MacOS
```
/etc/hosts
```
### For Windows
```
C:\Windows\System32\drivers\etc\hosts
```

Be aware that you will need to have root/admin permissions to update this file on your OS.
